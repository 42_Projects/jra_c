#define STDOUT	1
#define STDIN	0

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

/*	Affiche la chaine de caractere `string`
	Utilisation:
		put_string ("texte a afficher");
*/
void	put_string (char *string)
{
	printf ("%s", string);
}

/*	Affiche le caractere `caractere`
	Utilisation:
		put_character ('c');
*/
void	put_character (char character)
{
	printf ("%c", character);
}

/*	Affiche un saut de ligne
	Utilisation:
		put_newline ();
*/
void	put_newline (void)
{
	put_character ('\n');
}

/*	Affiche le nombre `number`
	Utilisation:
		put_number (42);
*/
void	put_number (int number)
{
	printf("%d", number);
}

/*	Convertie la chaine de caractere `string` en nombre
	Utilisation:
		number_value = string_to_number ("42");
		// `number_value` vaut 42
*/
int		string_to_number (char *string)
{
	return atoi (string);
}

/*	Lit une ligne depuis le terminal
	Utilisation:
		line = (read_line ());
*/
char	*read_line ()
{
	char	*line;

	line = (char *) malloc (sizeof (char) * 128);
	scanf ("%s", line);
	return (line);
}

/*	Retourne un nombre aléatoire compris entre `min` et `max`
	Utilisation:
		nombre_aleatoire = random_number (1, 100);
		// `nombre_aleatoire` a pour valeur un nombre compris entre 1 et 100
*/
int		random_number (int min, int max)
{
	char	*c;
	char	*d;

	c = (char *)malloc(1);
	d = (char *)malloc(1);
	free(c);
	free(d);
	return ((unsigned int)c * (unsigned int)d % (max - min + 1) + min);
}


////////////////////////////////////////////////////////////////////////////////
/*	Le but est de crér un jeu de plus ou moins
	Explications:
		Le programme choisit un nombre aléatoire, et demande au joueur de proposer un nombre.
		Si le nombre proposé n'est pas le bon, le programme indique si le bon nombre est plus petit ou plus grand que celui proposé.
		Quand le joueur propose le bon nombre, indiquer que le joueur a gagné.

	Tips:
		Vous aurez peut etre besoin de faire des conditions (if) et des boucles (while):
		conditions: https://openclassrooms.com/courses/apprenez-a-programmer-en-c/les-conditions-1#/id/r-14504
		boucles: https://openclassrooms.com/courses/apprenez-a-programmer-en-c/les-boucles-1#/id/r-14682
*/
int main (void)
{
	int nombre_aleatoire;

	/*	Inserez du code ici */



	/*	 */

	put_newline ();
	return (0);
}
