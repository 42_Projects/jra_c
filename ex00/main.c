#define STDOUT	1

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

/*	Affiche la chaine de caractere `string`
	Utilisation:
		put_string ("texte a afficher");
*/
void	put_string (char *string)
{
	printf ("%s", string);
}

/*	Affiche le caractere `caractere`
	Utilisation:
		put_character ('c');
*/
void	put_character (char character)
{
	printf ("%c", character);
}

/*	Affiche un saut de ligne
	Utilisation:
		put_newline ();
*/
void	put_newline (void)
{
	put_character ('\n');
}

////////////////////////////////////////////////////////////////////////////////
/*	Le but est d'afficher "Hello World!" */
int main (void) {
	/*	Inserez du code ici */



	/*	 */

	put_newline ();

	return (0);
}
