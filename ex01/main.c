#define STDOUT	1
#define STDIN	0

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

/*	Affiche la chaine de caractere `string`
	Utilisation:
		put_string ("texte a afficher");
*/
void	put_string (char *string)
{
	printf ("%s", string);
}

/*	Affiche le caractere `caractere`
	Utilisation:
		put_character ('c');
*/
void	put_character (char character)
{
	printf ("%c", character);
}

/*	Affiche un saut de ligne
	Utilisation:
		put_newline ();
*/
void	put_newline (void)
{
	put_character ('\n');
}

/*	Affiche le nombre `number`
	Utilisation:
		put_number (42);
*/
void	put_number (int number)
{
	printf("%d", number);
}

/*	Convertie la chaine de caractere `string` en nombre
	Utilisation:
		number_value = string_to_number ("42");
		// number_value vaut 42
*/
int		string_to_number (char *string)
{
	return atoi (string);
}

/*	Lit une ligne depuis le terminal
	Utilisation:
		line = (read_line ());
 */
char	*read_line ()
{
	char	*line;

	line = (char *) malloc (sizeof (char) * 128);
	scanf ("%s", line);
	return (line);
}

////////////////////////////////////////////////////////////////////////////////
/*	Le but est de demander a l'utilisateur de rentrer 2 nombres, et d'afficher leur somme. */
int main (void)
{
	int number_1;
	int number_2;
	int sum;

	/*	Inserez du code ici */



	/*	 */

	put_newline ();
	return (0);
}
